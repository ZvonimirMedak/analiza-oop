﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bDiv_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = mA / mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bMulti_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = mA * mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bMinus_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = mA - mB;
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bPlus_Click(object sender, EventArgs e)
        {
            try
            {
            double mA = double.Parse(tbFirstOperand.Text);
            double mB = double.Parse(tbSecondOperand.Text);
            double result = mA + mB;
            lbResult.Text = result.ToString();
            }catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            
                double result = 0;
                lbResult.Text = result.ToString();
            
        }

        private void bPow_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Pow(mA, mB);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bCos_Click(object sender, EventArgs e)
        {
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Acos(mA / mB);
                double result2 = Math.Cos(result);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bSin_Click(object sender, EventArgs e)
        {
            
            try
            {   double mA = double.Parse(tbFirstOperand.Text);
            double mB = double.Parse(tbSecondOperand.Text);
            double result = Math.Asin(mA / mB);
            double result2 = Math.Sin(result);
            lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bSqrt_Click(object sender, EventArgs e)
        {
            
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Sqrt(mA + mB);
                lbResult.Text = result.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid input");
            }
        }

        private void bLog_Click(object sender, EventArgs e)
        {
            
            try
            {
                double mA = double.Parse(tbFirstOperand.Text);
                double mB = double.Parse(tbSecondOperand.Text);
                double result = Math.Log(mA + mB);
                lbResult.Text = result.ToString();
            }
            catch(Exception)
            {
                MessageBox.Show("Invalid input");
            }
            
        }
    }
}

﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbResult = new System.Windows.Forms.Label();
            this.bDiv = new System.Windows.Forms.Button();
            this.bMulti = new System.Windows.Forms.Button();
            this.bMinus = new System.Windows.Forms.Button();
            this.bPlus = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bLog = new System.Windows.Forms.Button();
            this.bSqrt = new System.Windows.Forms.Button();
            this.bSin = new System.Windows.Forms.Button();
            this.bCos = new System.Windows.Forms.Button();
            this.bPow = new System.Windows.Forms.Button();
            this.tbFirstOperand = new System.Windows.Forms.TextBox();
            this.tbSecondOperand = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbResult
            // 
            this.lbResult.AutoSize = true;
            this.lbResult.Location = new System.Drawing.Point(469, 59);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(22, 13);
            this.lbResult.TabIndex = 1;
            this.lbResult.Text = "0.0";
            // 
            // bDiv
            // 
            this.bDiv.Location = new System.Drawing.Point(25, 149);
            this.bDiv.Name = "bDiv";
            this.bDiv.Size = new System.Drawing.Size(75, 23);
            this.bDiv.TabIndex = 2;
            this.bDiv.Text = "/";
            this.bDiv.UseVisualStyleBackColor = true;
            this.bDiv.Click += new System.EventHandler(this.bDiv_Click);
            // 
            // bMulti
            // 
            this.bMulti.Location = new System.Drawing.Point(144, 148);
            this.bMulti.Name = "bMulti";
            this.bMulti.Size = new System.Drawing.Size(75, 23);
            this.bMulti.TabIndex = 3;
            this.bMulti.Text = "*";
            this.bMulti.UseVisualStyleBackColor = true;
            this.bMulti.Click += new System.EventHandler(this.bMulti_Click);
            // 
            // bMinus
            // 
            this.bMinus.Location = new System.Drawing.Point(281, 148);
            this.bMinus.Name = "bMinus";
            this.bMinus.Size = new System.Drawing.Size(75, 23);
            this.bMinus.TabIndex = 4;
            this.bMinus.Text = "-";
            this.bMinus.UseVisualStyleBackColor = true;
            this.bMinus.Click += new System.EventHandler(this.bMinus_Click);
            // 
            // bPlus
            // 
            this.bPlus.Location = new System.Drawing.Point(416, 148);
            this.bPlus.Name = "bPlus";
            this.bPlus.Size = new System.Drawing.Size(75, 23);
            this.bPlus.TabIndex = 5;
            this.bPlus.Text = "+";
            this.bPlus.UseVisualStyleBackColor = true;
            this.bPlus.Click += new System.EventHandler(this.bPlus_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(533, 147);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 23);
            this.bClear.TabIndex = 6;
            this.bClear.Text = "CE";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bLog
            // 
            this.bLog.Location = new System.Drawing.Point(25, 205);
            this.bLog.Name = "bLog";
            this.bLog.Size = new System.Drawing.Size(75, 23);
            this.bLog.TabIndex = 7;
            this.bLog.Text = "log";
            this.bLog.UseVisualStyleBackColor = true;
            this.bLog.Click += new System.EventHandler(this.bLog_Click);
            // 
            // bSqrt
            // 
            this.bSqrt.Location = new System.Drawing.Point(144, 204);
            this.bSqrt.Name = "bSqrt";
            this.bSqrt.Size = new System.Drawing.Size(75, 23);
            this.bSqrt.TabIndex = 8;
            this.bSqrt.Text = "sqrt";
            this.bSqrt.UseVisualStyleBackColor = true;
            this.bSqrt.Click += new System.EventHandler(this.bSqrt_Click);
            // 
            // bSin
            // 
            this.bSin.Location = new System.Drawing.Point(281, 203);
            this.bSin.Name = "bSin";
            this.bSin.Size = new System.Drawing.Size(75, 23);
            this.bSin.TabIndex = 9;
            this.bSin.Text = "Sin";
            this.bSin.UseVisualStyleBackColor = true;
            this.bSin.Click += new System.EventHandler(this.bSin_Click);
            // 
            // bCos
            // 
            this.bCos.Location = new System.Drawing.Point(416, 204);
            this.bCos.Name = "bCos";
            this.bCos.Size = new System.Drawing.Size(75, 23);
            this.bCos.TabIndex = 10;
            this.bCos.Text = "Cos";
            this.bCos.UseVisualStyleBackColor = true;
            this.bCos.Click += new System.EventHandler(this.bCos_Click);
            // 
            // bPow
            // 
            this.bPow.Location = new System.Drawing.Point(533, 204);
            this.bPow.Name = "bPow";
            this.bPow.Size = new System.Drawing.Size(75, 23);
            this.bPow.TabIndex = 11;
            this.bPow.Text = "Pow";
            this.bPow.UseVisualStyleBackColor = true;
            this.bPow.Click += new System.EventHandler(this.bPow_Click);
            // 
            // tbFirstOperand
            // 
            this.tbFirstOperand.Location = new System.Drawing.Point(57, 52);
            this.tbFirstOperand.Name = "tbFirstOperand";
            this.tbFirstOperand.Size = new System.Drawing.Size(100, 20);
            this.tbFirstOperand.TabIndex = 12;
            // 
            // tbSecondOperand
            // 
            this.tbSecondOperand.Location = new System.Drawing.Point(231, 52);
            this.tbSecondOperand.Name = "tbSecondOperand";
            this.tbSecondOperand.Size = new System.Drawing.Size(100, 20);
            this.tbSecondOperand.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tbSecondOperand);
            this.Controls.Add(this.tbFirstOperand);
            this.Controls.Add(this.bPow);
            this.Controls.Add(this.bCos);
            this.Controls.Add(this.bSin);
            this.Controls.Add(this.bSqrt);
            this.Controls.Add(this.bLog);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bPlus);
            this.Controls.Add(this.bMinus);
            this.Controls.Add(this.bMulti);
            this.Controls.Add(this.bDiv);
            this.Controls.Add(this.lbResult);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbResult;
        private System.Windows.Forms.Button bDiv;
        private System.Windows.Forms.Button bMulti;
        private System.Windows.Forms.Button bMinus;
        private System.Windows.Forms.Button bPlus;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bLog;
        private System.Windows.Forms.Button bSqrt;
        private System.Windows.Forms.Button bSin;
        private System.Windows.Forms.Button bCos;
        private System.Windows.Forms.Button bPow;
        private System.Windows.Forms.TextBox tbFirstOperand;
        private System.Windows.Forms.TextBox tbSecondOperand;
    }
}

